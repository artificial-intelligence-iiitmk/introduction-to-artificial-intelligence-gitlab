# Introduction to Artificial Intelligence GitLab

  

This repo will contain all the relevant information for the usage of the projects in this group and a brief description of how we use Git here.
## How projects are stored
At any point of time there will be two copies of the project. 
* **master** Branch   : This copy will be visible to everyone and will be the official copy. Only be editable by the _Owner_. 
* **developer** / **working** Branch: This will be used by everyone to work on the project and make changes. Only editable by _Maintainer_ 

## How to use this repo?

  

1. Fork any project to your profile

1. Clone the forked repo and make any changes in your machine

1. Push the changes onto your fork

### For Developers
* Submit a merge request from the forker repo on your profile to the **developer** branch 
### For Maintainers
* Accept the merge requests from _Developers_ to the **developer** branch if the changes are authentic and proper
* Submit a merge request from the **developer** branch to the **master** branch
### For Owners
* Accept the merge requests from _Maintainers_ to the **master* branch if the changes are authentic and proper

## Let's get familiar with GitLab
1.   Fork this [repo]([https://gitlab.com/artificial-intelligence-iiitmk/introduction-to-artificial-intelligence-gitlab](https://gitlab.com/artificial-intelligence-iiitmk/introduction-to-artificial-intelligence-gitlab)) 
2. Clone the forked repo using 
`git clone https://gitlab.com/artificial-intelligence-iiitmk/introduction-to-artificial-intelligence-gitlab.git`
3. Go the cloned file 
    `cd introduction-to-artificial-intelligence-gitlab`
  1. Go to member folder 
  `cd member`
  2. Create a folder under your name  and in the folder create a readme file titled `your_name.txt` and in the _your_name.txt_ put your email 
  3. Add the changes you have made 
  `git add .`
  4. Commit your changes 
  `git commit -m "some message here"`
  5. Push your changes to your profile
  `git push origin master`
  6. Go to your profile and the changes should have been made. 
  7. Create a merge request for _introduction-to-artificial-intelligence-gitlab_ in your profile to _artificial-intelligence-iiitmk/introduction-to-artificial-intelligence-gitlab_ **master** branch
  8. After the merge request has been accepted you can see your folder in the master branch of [artificial-intelligence-iiitmk/introduction-to-artificial-intelligence-gitlab]([https://gitlab.com/artificial-intelligence-iiitmk/introduction-to-artificial-intelligence-gitlab](https://gitlab.com/artificial-intelligence-iiitmk/introduction-to-artificial-intelligence-gitlab))


### If you have any doubts or quries regarding projects or GitLab you may contact me, Anandha Krishnan H anandha.mi3@iiitmk.ac.in
## Have Fun and May the Force Be with You ; ) 
																		