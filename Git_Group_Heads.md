|**Groups**                  |**Heads**                    |
|----------------------------|:---------------------:|
|[**Cryptography**](https://gitlab.com/cryptography-iiitmk)            |**Ashok**              |
|[**Hacking**](https://gitlab.com/hacking-iiitmk)                 | **Visakh**            |
|[**Blockchain**](https://gitlab.com/blockchain-iiitmk)              | **Pounraj**           |
|[**Malware Analysis**](https://gitlab.com/malware-analysis-iiitmk)        | **Devyani Vij**       |
|[**Artificial Intelligence**](https://gitlab.com/artificial-intelligence-iiitmk) | [**Anandha Krishnan H**](https://gitlab.com/AKing1998?nav_source=navbar)|
|[**Natural Language Processing**](https://gitlab.com/nlp-iiitmk)                     | **Sukesh Shenoy**     |
|[**Programming**](https://gitlab.com/programming-iiitmk)             | **Olivi T J**         |
|[**Android**](https://gitlab.com/android-iiitmk)                 | **Kevin**             |
|[**Data Analytics**](https://gitlab.com/data-analytics-iiitmk)          | **Pallavi**           |
|[**Machine Learning**](https://gitlab.com/machine-learning-iiitmk)        | **Prince M S**        |
|[**Computer Vision**](https://gitlab.com/computer-vision-iiitmk)         | **Nitish Tom Michael**|
|[**Big Data**](https://gitlab.com/big-data-iiitmk)                | **Sandeep**           |
|[**GIS**](https://gitlab.com/gis-iiitmk)                     | **Jeffin**            |
|[**Web Development**](https://gitlab.com/web-development-iiitmk)         | **Akhil KK**          |
|[**Statistics**](https://gitlab.com/web-development-iiitmk)              | **Lipsa Alvin Johnny**|
